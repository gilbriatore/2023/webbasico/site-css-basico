# Site CSS I Básico


## Exemplo de uso das técnicas: inline, incorporado e folhas

Inline: os estilos são aplicados diretamente na tag html.

Incorporado: os estilos são escritos no cabeçalho da página (head), dentro da tag (style).

Folha: os estilos são escritos em um arquivo css em separado da página html.